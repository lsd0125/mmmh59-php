<?php
if(! isset($pageName)){
    $pageName = '';
}
?>
<style>
    .navbar .nav-item.active {
        border: #005cbf 2px solid;
        border-radius: 10px;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= $pageName==='ab-list' ? 'active' : '' ?>">
                    <a class="nav-link" href="ab-list.php">通訊錄列表</a>
                </li>
                <li class="nav-item <?= $pageName==='ab-insert' ? 'active' : '' ?>">
                    <a class="nav-link" href="ab-insert.php">新增通訊</a>
                </li>
                <?php if(! isset($_SESSION['admin'])): ?>
                    <li class="nav-item <?= $pageName==='ab-login' ? 'active' : '' ?>">
                        <a class="nav-link" href="ab-login.php">ab login</a>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link"><?= $_SESSION['admin']['nickname'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ab-logout.php" >登出</a>
                    </li>
                <?php endif ?>

            </ul>

        </div>
    </div>
</nav>