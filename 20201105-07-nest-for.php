<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td {
            width: 40px;
            height: 40px;
            color: white;
        }
        td.first {
            /*background-color: rgb(181, 66, 66);*/
            background-color: hsl(240,100%, 50%);
        }

    </style>
</head>
<body>
<table border="1">
    <?php for($k=0; $k<16; $k++): ?>
    <tr>
        <?php for($i=0; $i<16; $i++):
            $c = sprintf('#0%X%X', $k, $i);
            ?>
           <td style="background-color:<?= $c ?>">
            <?= $c ?>
           </td>
         <?php endfor ?>
    </tr>
    <?php endfor ?>
</table>
<table>
    <tr>
        <td class="first">
            123
        </td>
    </tr>
</table>

</body>
</html>