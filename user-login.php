<?php include __DIR__. '/parts/config.php'; ?>
<?php
if(isset($_SESSION['user'])){
    header('Location: product-list.php');
    exit;
}

if(isset($_SERVER['HTTP_REFERER'])){
    $gotoURL = $_SERVER['HTTP_REFERER'];
} else {
    $gotoURL = 'product-list.php';
}
?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/scripts.php'; ?>
<?php include __DIR__. '/parts/navbar-proj.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div id="info_bar" class="alert alert-danger" role="alert" style="display: none">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">會員登入</h5>

                    <form name="form1" onsubmit="checkForm(); return false;">
                        <div class="form-group">
                            <label for="email">email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   >
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                            >
                            <small class="form-text"></small>
                        </div>
                        <button type="submit" class="btn btn-primary">登入</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>

<script>
    const email = $('#email'),
        password = $('#password'),
        info_bar = $('#info_bar')

    function checkForm(){

        $.post('user-login-api.php', {email: email.val(), password: password.val()}, function(data){
            if(data.success){
                info_bar
                    .removeClass('alert-danger')
                    .addClass('alert-success')
                    .text('登入成功');
                location.href = '<?= $gotoURL ?>';
            } else {
                info_bar
                    .removeClass('alert-success')
                    .addClass('alert-danger')
                    .text('登入失敗');
            }
            info_bar.slideDown();

            setTimeout(function(){
                info_bar.slideUp();
            }, 2000);
        }, 'json');
    }

</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
