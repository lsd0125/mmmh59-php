<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php

$list = [
    [
        'name' => 'Bill',
        'age' => 20,
        'gender' => 'male',
    ],
    [
        'name' => 'Peter',
        'age' => 32,
        'gender' => 'male',
    ],
    [
        'name' => 'Flora',
        'age' => 26,
        'gender' => 'female',
    ]
];

?>

<table border="1">
    <tr>
        <td>姓名</td>
        <td>性別</td>
        <td>年齡</td>
    </tr>
    <?php foreach($list as $item): ?>
        <tr>
            <td><?= $item['name'] ?></td>
            <td><?= $item['gender'] ?></td>
            <td><?= $item['age'] ?></td>
        </tr>
    <?php endforeach;  ?>
</table>



</body>
</html>