<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$age = $_GET['age'] ?? 0;
$age = intval($age);
?>
<?php if($age >= 18): ?>
    <img src="imgs/cat01.jpg" alt="">
    <p>老貓</p>
<?php else: ?>
    <img src="imgs/cat02.jpg" alt="">
    <p>小貓</p>
<?php endif; ?>
</body>
</html>