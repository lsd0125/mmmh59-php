
SELECT * FROM `products` JOIN `categories`;

SELECT * FROM `products` JOIN `categories` ON `products`.`category_sid`=`categories`.`sid`;

SELECT `categories`.name, `products`.*  FROM `products` JOIN `categories` ON `products`.`category_sid`=`categories`.`sid`;

SELECT c.name, p.*  FROM `products` AS p JOIN `categories` AS c ON p.`category_sid`=c.`sid`;

-- inner join
SELECT c.name, p.*  FROM `products` p JOIN `categories` c ON p.`category_sid`=c.`sid`;
SELECT c.name, p.*  FROM `categories` c JOIN `products` p ON p.`category_sid`=c.`sid`;

-- outer join
SELECT c.name, p.*  FROM `products` p LEFT JOIN `categories` c ON p.`category_sid`=c.`sid`;
SELECT c.name, p.*  FROM `categories` c LEFT JOIN `products` p ON p.`category_sid`=c.`sid`;

--
SELECT c.name cate_name, p.*  FROM `products` p JOIN `categories` c ON p.`category_sid`=c.`sid`;

--
SELECT * FROM `products` WHERE `author` LIKE '%林%';

SELECT * FROM `products`
    WHERE
        `author` LIKE '%林%'
        AND
        `author` NOT LIKE '林%';


SELECT * FROM `products` WHERE sid IN (10, 14, 21, 6) ORDER BY RAND();

--
SELECT COUNT(1) num FROM `products`;

SELECT * FROM `products` GROUP BY `category_sid`;

SELECT *, COUNT(1) num FROM `products` GROUP BY `category_sid`;

SELECT c.name, COUNT(1) num FROM `products` p
    JOIN categories c
    ON p.category_sid = c.sid
    GROUP BY p.`category_sid`;

-- CRUD: create, read, update, delete








