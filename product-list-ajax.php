<?php include __DIR__. '/parts/config.php'; ?>
<?php

$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;

$c_sql = "SELECT * FROM categories WHERE parent_sid=0";
$c_rows = $pdo->query($c_sql)->fetchAll();


?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/navbar-proj.php'; ?>
<div class="container">


    <div class="row">
        <div class="col-md-3">
            <div class="btn-group-vertical" style="width: 100%">
                <button type="button" class="cate-btn btn btn-outline-primary"
                        data-sid="0">所有商品</button>
                <?php foreach($c_rows as $c): ?>
                    <button type="button" class="cate-btn btn btn-outline-primary"
                            data-sid="<?= $c['sid'] ?>">
                        <?= $c['name'] ?>
                    </button>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row product-list">
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/parts/scripts.php'; ?>
<script>
    const cate_btns = $('.cate-btn');

    const productTpl = function(a){
        return `
            <div class="col-md-3">
                <div class="card"">
                    <img src="imgs/big/${a.book_id}.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h6 class="card-title">${a.bookname}</h6>
                        <p><i class="fas fa-user-edit"></i>${a.author}</p>
                        <p><i class="fas fa-dollar-sign"></i></i>${a.price}</p>
                        <select class="form-control" style="display: inline-block; width: auto">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <button class="btn btn-primary"><i class="fas fa-cart-plus"></i></button>
                    </div>
                </div>
            </div>
        `;
    };

    function whenHashChanged(){
        let u = parseInt(location.hash.slice(1)) || 0;
        console.log(u);
        getProductData(u);

        cate_btns.removeClass('btn-primary').addClass('btn-outline-primary');
        cate_btns.each(function(index, el){
            const sid = parseInt($(this).attr('data-sid'));
            if(sid===u){
                $(this).removeClass('btn-outline-primary').addClass('btn-primary');
            }
        });
    }
    window.addEventListener('hashchange', whenHashChanged);
    whenHashChanged();

    cate_btns.on('click', function(event){
        const sid = $(this).attr('data-sid') *1;
        console.log(`sid: ${sid}`);
        location.href = "#" + sid;
    });

    function getProductData(cate=0){
        $.get('product-list-ajax-api.php', {cate: cate}, function(data){
            console.log(data);

            let str = '';
            if(data.products && data.products.length){
                data.products.forEach(function(el){
                    str += productTpl(el);
                });
            }
            $('.product-list').html(str);
        }, 'json');
    }

</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
