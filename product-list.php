<?php include __DIR__. '/parts/config.php'; ?>
<?php
$params = [];
$perPage = 4; // 每頁幾筆
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;

$c_sql = "SELECT * FROM categories WHERE parent_sid=0";
$c_rows = $pdo->query($c_sql)->fetchAll();

$where = " WHERE 1 ";
if(! empty($cate)){
    $params['cate'] = $cate;
    $where .= " AND `category_sid`=$cate ";
}


$t_sql = "SELECT COUNT(1) FROM products $where ";
$t_stmt = $pdo->query($t_sql);

//echo json_encode($t_stmt->fetch(PDO::FETCH_NUM)[0]); exit;
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 總筆數
$totalPages = ceil($totalRows/$perPage); // 總頁數
if($totalRows!=0) {
    if ($page < 1) {
        header('Location: product-list.php');
        exit;
    }
    if ($page > $totalPages) {
        header('Location: product-list.php?page=' . $totalPages);
        exit;
    }
    $sql = sprintf("SELECT * FROM products %s ORDER BY sid DESC LIMIT %s, %s",
        $where,
        ($page - 1) * $perPage,
        $perPage);

    $stmt = $pdo->query($sql);

    $rows = $stmt->fetchAll();
} else {
    $rows = [];
}



?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/scripts.php'; ?>
<?php include __DIR__. '/parts/navbar-proj.php'; ?>
<div class="container">
<!--    <p>-->
<!--        --><?//= json_encode($rows, JSON_UNESCAPED_UNICODE); ?>
<!--    </p>-->

    <div class="row">
        <div class="col-md-3">
            <div class="btn-group-vertical" style="width: 100%">
                <a type="button" class="btn btn<?= empty($cate) ? '' : '-outline' ?>-primary"
                   href="product-list.php">所有商品</a>
                <?php foreach($c_rows as $c): ?>
                    <a type="button" class="btn btn<?= $cate==$c['sid'] ? '' : '-outline' ?>-primary"
                       href="?cate=<?= $c['sid'] ?>">
                        <?= $c['name'] ?>
                    </a>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                                <a class="page-link" href="?<?php
                                $params['page']=$page-1;
                                echo http_build_query($params);
                                ?>">
                                    <i class="fas fa-arrow-circle-left"></i>
                                </a>
                            </li>
                            <?php for($i=$page-2; $i<=$page+2; $i++): ?>
                                <?php if($i>=1 and $i<=$totalPages): ?>
                                    <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                                        <a class="page-link" href="?<?php
                                        $params['page']=$i;
                                        echo http_build_query($params);
                                        ?>"><?= $i ?></a>
                                    </li>
                                <?php endif ?>
                            <?php endfor ?>
                            <li class="page-item <?= $page==$totalPages ? 'disabled' : '' ?>">
                                <a class="page-link" href="?<?php
                                $params['page']=$page+1;
                                echo http_build_query($params);
                                ?>">
                                    <i class="fas fa-arrow-circle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>


            <div class="row">
                <?php foreach($rows as $r):  ?>
                <div class="col-md-3 product-item" data-sid="<?= $r['sid'] ?>">

                    <div class="card">
                        <a href="product-detail02.php?sid=<?= $r['sid'] ?>" target="_blank">
                            <img src="imgs/big/<?= $r['book_id'] ?>.png" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <h6 class="card-title">
                                <a href="javascript: showProductModal(<?= $r['sid'] ?>)">
                                    <?= $r['bookname'] ?>
                                </a>
                            </h6>
                            <p><i class="fas fa-user-edit"></i><?= $r['author'] ?></p>
                            <p><i class="fas fa-dollar-sign"></i></i><?= $r['price'] ?></p>
                            <select class="quantity form-control" style="display: inline-block; width: auto">
                                <?php for($i=1; $i<=20; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor ?>
                            </select>
                            <button class="btn btn-primary buy-btn"><i class="fas fa-cart-plus"></i></button>
                        </div>
                    </div>
                </div>
                <?php endforeach;  ?>
            </div>





        </div>
    </div>
    <div class="row">
        <div class="col">

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <iframe src="product-detail02.php?sid=17" style="width: 100%; height: 100%;">

                            </iframe>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.buy-btn').on('click', function(event){
        const item  = $(this).closest('.product-item');
        const sid = item.attr('data-sid');
        const qty = item.find('.quantity').val();

        console.log({sid:sid, quantity: qty});
        $.get('handle-cart.php', {sid:sid, quantity: qty, action:'add'}, function(data){
            console.log(data);
            countCart(data.cart);
        }, 'json');
    });

    function showProductModal(sid){
        $('iframe')[0].src = "product-detail02.php?sid=" + sid;
        // product-detail02.php?sid=17
        $('#exampleModal').modal('show')
    }
</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
