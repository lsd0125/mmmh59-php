<?php include __DIR__. '/parts/config.php'; ?>
<?php
if(isset($_POST['account']) and isset($_POST['password'])){
    if($_POST['account']==='shin' and $_POST['password']==='1234') {
        $_SESSION['user'] = [
                'account' => 'shin',
                'nickname' => '小新',
        ];
    } else {
        $msg = '帳號或密碼錯誤';
    }
}
?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/navbar.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <?php if(isset($msg)): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $msg ?>
                </div>
            <?php endif; ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <?php if(isset($_SESSION['user'])): ?>
                    <h6><?= $_SESSION['user']['nickname'] ?> 您好</h6>
                        <p><a href="20201110-02-logout.php">登出</a></p>
                    <?php else:  ?>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="account">account</label>
                            <input type="text" class="form-control" id="account" name="account"
                                   >
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   >
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="my_check" value="是">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <?php endif;  ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/parts/scripts.php'; ?>
<?php include __DIR__. '/parts/html-foot.php'; ?>
