<?php include __DIR__. '/parts/config.php'; ?>
<?php
if(isset($_SESSION['admin'])){
    header('Location: ab-list.php');
    exit;
}
?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/navbar.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div id="info_bar" class="alert alert-danger" role="alert" style="display: none">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">管理通訊錄-登入</h5>

                    <form name="form1" onsubmit="checkForm(); return false;">
                        <div class="form-group">
                            <label for="account">account</label>
                            <input type="text" class="form-control" id="account" name="account"
                                   >
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                            >
                            <small class="form-text"></small>
                        </div>
                        <button type="submit" class="btn btn-primary">登入</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/parts/scripts.php'; ?>
<script>
    const account = $('#account'),
        password = $('#password'),
        info_bar = $('#info_bar')

    function checkForm(){

        $.post('ab-login-api.php', {account: account.val(), password: password.val()}, function(data){
            if(data.success){
                info_bar
                    .removeClass('alert-danger')
                    .addClass('alert-success')
                    .text('登入成功');
                location.href = 'ab-list.php';
            } else {
                info_bar
                    .removeClass('alert-success')
                    .addClass('alert-danger')
                    .text('登入失敗');
            }
            info_bar.slideDown();

            setTimeout(function(){
                info_bar.slideUp();
            }, 2000);
        }, 'json');
    }

</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
