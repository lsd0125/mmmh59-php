<?php
$age = isset($_GET['age']) ? intval($_GET['age']) : 0; // 傳統的寫法
//$age = $_GET['age'] ?? 0;  // php7 之後提供的功能
echo $age;


// isset() 判斷變數有沒有設定
// intval() 把字串轉換成整數
// floatval()