<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar = array(4,5,6);
$ar2 = [7,8,9];

$ar[] = 12;  // array push

print_r($ar);

var_dump($ar2);

?>
</pre>
</body>
</html>