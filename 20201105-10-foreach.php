<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar = array(
    'name' => 'shin',
    'age' => 20,
    'gender' => 'male',
);
$ar2 = [3,5,7,9,13];

foreach($ar as $k=>$v) {
    echo "$k : $v <br>";
}

echo '-----------<br>';

foreach($ar2 as $v) {
    echo "$v <br>";
}

?>
</pre>
</body>
</html>