<?php
include __DIR__. '/parts/config.php';

?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/scripts.php'; ?>
<?php include __DIR__. '/parts/navbar-proj.php'; ?>
<div class="container">
    <?php if(empty($_SESSION['cart'])): ?>
        <div class="alert alert-danger" role="alert">
            購物車沒有內容, 請到商品頁購物!
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">
                            <i class="fas fa-trash-alt"></i>
                        </th>
                        <th scope="col">書名</th>
                        <th scope="col">封面</th>
                        <th scope="col">價格</th>
                        <th scope="col">數量</th>
                        <th scope="col">小計</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($_SESSION['cart'] as $i): ?>
                    <tr class="product-item" data-sid="<?= $i['sid'] ?>" id="prod<?= $i['sid'] ?>">
                        <td><a href="javascript:removeItem(<?= $i['sid'] ?>)">
                            <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                        <td><?= $i['bookname'] ?></td>
                        <td><img src="imgs/small/<?= $i['book_id'] ?>.jpg" alt=""></td>
                        <td class="price" data-price="<?= $i['price'] ?>">
                            $ <?= $i['price'] ?>
                        </td>
                        <td class="quantity" data-quantity="<?= $i['quantity'] ?>">
                            <select class="quantity form-control" style="display: inline-block; width: auto">
                                <?php for($k=1; $k<=20; $k++): ?>
                                    <option value="<?= $k ?>"><?= $k ?></option>
                                <?php endfor ?>
                            </select>
                        </td>
                        <td class="subtotal">0</td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="alert alert-primary" role="alert">
                    總計: <span id="totalAmount"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php if(isset($_SESSION['user'])): ?>
                <button class="btn btn-success" onclick="doBuy()">結帳</button>
                <?php else: ?>
                <button class="btn btn-danger disabled">請先登入會員</button>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<script>
const dallorCommas = function(n){
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
};
function removeItem(sid){
    $.get('handle-cart.php', {sid:sid, action:'remove'}, function(data){
        console.log(data);
        countCart(data.cart);
        $('#prod'+sid).remove();

        calcTotal()
    }, 'json');
}

function calcTotal(){
    let total = 0;
    $('.product-item').each(function(){
        const tr = $(this);
        const price = parseInt( tr.find('td.price').attr('data-price') );
        const quantity = parseInt( tr.find('td.quantity').attr('data-quantity') );
        tr.find('td.quantity > select').val(quantity);
        tr.find('.subtotal').text('$ ' + dallorCommas(price*quantity));
        total += price*quantity;
    });
    $('#totalAmount').text('$ ' + dallorCommas(total));
}
calcTotal();


$('.product-item td.quantity > select').on('change', function(){
    const tr = $(this).closest('.product-item');
    const quantity = $(this).val();
    const sid = tr.attr('data-sid');
    const combo = $(this);
    $.get('handle-cart.php', {sid, action:'add', quantity}, function(data){
        console.log(data);
        countCart(data.cart);
        combo.closest('td.quantity').attr('data-quantity', quantity);
        calcTotal();
    }, 'json');
});

function doBuy(){
    $.get('buy.php', function(data){
        if(data.success){
            alert('感謝訂購~');
            location.reload(); // 重新載入頁面
        } else {
            console.log(data);
        }
    }, 'json');

}


</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
