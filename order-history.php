<?php
include __DIR__. '/parts/config.php';

if(! isset($_SESSION['user'])){
    header('Location: user-login.php');
    exit;
}

$member_sid = intval($_SESSION['user']['id']);
$o_sql = "SELECT * FROM `orders` WHERE `member_sid`=$member_sid ORDER BY `order_date` DESC";
$o_rows = $pdo->query($o_sql)->fetchAll();

// 如果沒有任何的訂購資料, 就顯示訊息或離開
if(empty($o_rows)){
    header('Location: product-list.php'); // 顯示訊息比較好, 告訴用戶沒有訂單資料
    exit;
}

$order_sids = [];
foreach($o_rows as $o){
    $order_sids[] = $o['sid'];
}

$d_sql = sprintf("SELECT d.*, p.bookname, p.book_id FROM `order_details` d 
JOIN `products` p ON p.sid=d.product_sid
WHERE d.`order_sid` IN (%s)", implode(',', $order_sids));

$d_rows = $pdo->query($d_sql)->fetchAll();

//echo json_encode([
//    'orders' => $o_rows,
//    'details' => $d_rows,
//]);
//exit;
?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/scripts.php'; ?>
<?php include __DIR__. '/parts/navbar-proj.php'; ?>
<div class="container">

        <div class="row">
            <div class="col">

                <table class="table table-bordered">

                    <tbody>
                    <?php foreach($o_rows as $o): ?>
                    <tr style="background-color: #a9cfd0;">
                        <td>訂單編號: <?= $o['sid'] ?></td>
                        <td>總金額: <?= $o['amount'] ?></td>
                        <td>訂購時間: <?= $o['order_date'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>商品名</td>
                                <td>書號</td>
                                <td>價格</td>
                                <td>數量</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($d_rows as $d): ?>
                                <?php if($o['sid']==$d['order_sid']): ?>
                                    <tr>
                                        <td><?= $d['product_sid'] ?></td>
                                        <td><?= $d['bookname'] ?></td>
                                        <td><?= $d['book_id'] ?></td>
                                        <td><?= $d['price'] ?></td>
                                        <td><?= $d['quantity'] ?></td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>


</div>
<script>
const dallorCommas = function(n){
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
};

</script>
<?php include __DIR__. '/parts/html-foot.php'; ?>
