<?php include __DIR__. '/parts/config.php'; ?>
<?php
require __DIR__. '/parts/admin-required.php';
// $pageName = 'ab-edit';
if(isset($_GET['sid'])){
    $sid =  intval($_GET['sid']);
} else {
    header('Location: ab-list.php'); exit;
}

$sql = "SELECT * FROM `address_book` WHERE sid=$sid";
$row = $pdo->query($sql)->fetch();

if(empty($row)){
    header('Location: ab-list.php'); exit;
}

//echo json_encode($row); exit;




?>
<?php include __DIR__. '/parts/html-head.php'; ?>
<?php include __DIR__. '/parts/navbar.php'; ?>
<style>
    small.form-text {
        color: red;
    }

</style>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div id="info_bar" class="alert alert-danger" role="alert" style="display: none">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">編輯通錄資料</h5>

                    <form name="form1" onsubmit="checkForm(); return false;" novalidate>
                        <input type="hidden" name="sid" value="<?= $row['sid'] ?>">
                        <div class="form-group">
                            <label for="name">** name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   value="<?= htmlentities($row['name']) ?>"
                                   required>
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="<?= htmlentities($row['email']) ?>">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">mobile</label>
                            <input type="text" class="form-control" id="mobile" name="mobile"
                                   value="<?= htmlentities($row['mobile']) ?>"
                                   pattern="09\d{2}-?\d{3}-?\d{3}">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">birthday</label>
                            <input type="date" class="form-control" id="birthday"
                                   value="<?= htmlentities($row['birthday']) ?>"
                                   name="birthday" >
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="address">address</label>
                            <textarea class="form-control" id="address" name="address" cols="30" rows="3"
                            ><?= htmlentities($row['address']) ?></textarea>
                            <small class="form-text"></small>
                        </div>


                        <button type="submit" class="btn btn-primary">修改</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/parts/scripts.php'; ?>
<script>
    const email_re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    const mobile_re = /^09\d{2}-?\d{3}-?\d{3}$/;
    const name = $('#name'),
        email = $('#email'),
        info_bar = $('#info_bar')

    function checkForm() {
        name.next().text('');
        email.next().text('');

        let isPass = true;

        if(name.val().length < 2){
            isPass = false;
            name.next().text('請填寫正確的姓名!');
        }
        // https://github.com/shinder/mmmh57-php/blob/master/proj/login.php

        if(email.val()){
            if(! email_re.test(email.val())){
                isPass = false;
                email.next().text('請填寫正確的 email 格式!');
            }
        }

        if(isPass){
            $.post('ab-edit-api.php', $(document.form1).serialize(), function(data){
                console.log(data);
                if(data.success){
                    info_bar
                        .removeClass('alert-danger')
                        .addClass('alert-success')
                        .text('修改完成');
                } else {
                    info_bar
                        .removeClass('alert-success')
                        .addClass('alert-danger')
                        .text(data.error || '資料沒有修改');
                }
                info_bar.slideDown();

                setTimeout(function(){
                    info_bar.slideUp();
                }, 2000);
            }, 'json')
        }


    }



</script>

<?php include __DIR__. '/parts/html-foot.php'; ?>
