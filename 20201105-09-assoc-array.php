<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar = array(
    'name' => 'shin',
    'age' => 20
);
$ar2 = [
    'name' => 'david',
    'age' => 25,
];

$ar3 = $ar2;  // duplicate

$ar2['name'] = 'flora';

print_r($ar);
print_r($ar2);
print_r($ar3);

?>
</pre>
</body>
</html>