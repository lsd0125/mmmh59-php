<?php
require __DIR__. '/parts/config.php';
require __DIR__. '/parts/admin-required.php';
$output = [
    'success' => false,
    'code' => 0,
    'error' => '沒有表單資料',
];

if(empty($_POST['name'])){
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}
// TODO: 檢查資料格式

$sql = "UPDATE `address_book` SET 
    `name`=?,
    `email`=?,
    `mobile`=?,
    `birthday`=?,
    `address`=?
 WHERE `sid`=?";

$stmt = $pdo->prepare($sql);
$stmt->execute([
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address'],
        $_POST['sid'],
]);
if($stmt->rowCount()==1){
    $output['success'] = true;
    $output['error'] = '';
} else {
    $output['error'] = '資料沒有變更';
}
echo json_encode($output, JSON_UNESCAPED_UNICODE);



